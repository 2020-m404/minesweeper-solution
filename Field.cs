﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Minesweeper_Solution
{
    class Field
    {
        /// <summary>
        /// Positional information for the field.
        /// </summary>
        public Cell Cell { get; }

        /// <summary>
        /// Convenience read-only property for the column of the fields cell.
        /// </summary>
        public int Column => Cell.Column;

        /// <summary>
        /// Convenience read-only property for the row of the fields cell.
        /// </summary>
        public int Row => Cell.Row;

        /// <summary>
        /// Count of mines in the surrounding fields.
        /// </summary>
        public int SurroundingMineCount { get; set; } = 0;

        /// <summary>
        /// Indicator whether this field is covered or uncovered.
        /// </summary>
        public bool IsUncovered { get; private set; }

        /// <summary>
        /// Flag for fields containing a mine.
        /// </summary>
        public bool IsMine { get; set; }

        /// <summary>
        /// Flag for fields suspected (by the player) to contain a mine.
        /// </summary>
        public bool IsSuspectedMine { get; private set; }

        /// <summary>
        /// Convenience read-only property to determine whether this field is neutral.
        /// A field is considered neutral if neither itself nor one of its surrounding fields does contain a mine.
        /// </summary>
        public bool IsNeutral => !IsMine && SurroundingMineCount == 0;

        /// <summary>
        /// Reference on the button which visually represents this field on the game board.
        /// </summary>
        public Button Button { get; set; }

        /// <summary>
        /// Constructor for objects of Field class.
        /// Fields can only be instantiated for a given position, represented by the cell.
        /// </summary>
        /// <param name="cell"></param>
        public Field(Cell cell)
        {
            Cell = cell;
        }

        /// <summary>
        /// Uncover this field.
        /// Method is called either when the button representing this field was clicked
        /// or when a surrounding, neutral field of a safe field was uncovered.
        /// </summary>
        public void Uncover()
        {
            // Updated fields properties according to its uncovering.
            IsUncovered = true;
            IsSuspectedMine = false;

            // Update the visual appearance of the field.
            string imageFileName = IsMine ? "mine-2.png" : $"{SurroundingMineCount}.png";
            Button.Content = GetImage(imageFileName);
            Button.IsEnabled = false;
        }

        /// <summary>
        /// Toggle the suspected-mine state of the field.
        /// </summary>
        public void ToggleSuspectMine()
        {
            // Inverse the boolean value
            IsSuspectedMine = !IsSuspectedMine;

            // Update the visual appearance of the field.
            string imageFileName = IsSuspectedMine ? "flag.png" : "button.png";
            Button.Content = GetImage(imageFileName);
        }

        /// <summary>
        /// Helper method which returns an Image object for the given filename.
        /// The images to be created and returned by this method must be located in the
        /// Resources/Images directory of the solution.
        /// </summary>
        /// <param name="filename">Last portion of the relative path.</param>
        /// <returns>An image built based on the given filename.</returns>
        private Image GetImage(string filename)
        {
            ImageSource source = new BitmapImage(new Uri($@"Resources\Images\{filename}", UriKind.Relative));
            return new Image { Source = source };
        }
    }
}
