﻿namespace Minesweeper_Solution
{
    /// <summary>
    /// A cell represents a location on the game board identified by its column and row.
    /// </summary>
    class Cell
    {
        /// <summary>
        /// The character used to separate column and row index in string representations of a cell object.
        /// </summary>
        private const char CoordinatesSeparator = '_';

        /// <summary>
        /// Column in which the cell is located.
        /// </summary>
        public int Column { get; }

        /// <summary>
        /// Row in which the cell is located.
        /// </summary>
        public int Row { get; }

        /// <summary>
        /// Constructor for cell object identified by a column and row number.
        /// </summary>
        /// <param name="column">Index of column in which the cell is located.</param>
        /// <param name="row">Index of row in which the row is located.</param>
        public Cell(int column, int row)
        {
            Column = column;
            Row = row;
        }

        /// <summary>
        /// Constructor for cell object described by a string containing the column and row indices.
        /// The format of the string must follow the pattern produced by the ToString method of this class.
        /// </summary>
        /// <param name="coordinates">String representation of the location.</param>
        public Cell(string coordinates)
        {
            string[] columnAndRow = coordinates.Split(CoordinatesSeparator);
            Column = int.Parse(columnAndRow[0]);
            Row = int.Parse(columnAndRow[1]);
        }

        /// <summary>
        /// Checks if the given cells location is identical to the current cell objects location.
        /// </summary>
        /// <param name="cell">The cell object to compare with.</param>
        /// <returns>True, if the two cells location are equal, False otherwise.</returns>
        public bool IsSamePosition(Cell cell)
        {
            return Column == cell.Column && Row == cell.Row;
        }

        /// <summary>
        /// Produces a string representation of the cell object consisting of the Column and Row values
        /// separated by a designated character value.
        /// </summary>
        /// <returns>String representation of the cell object.</returns>
        public new string ToString()
        {
            return $"{Column}{CoordinatesSeparator}{Row}";
        }

    }
}
