﻿using System;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Minesweeper_Solution
{
    /// <summary>
    /// The MainWindow class contains the code to draw and update the user interface.
    /// The UI class should contain as little business logic as possible.
    /// </summary>
    public partial class MainWindow : Window
    {
        // Reference to the board instance which holds all fields and most of the game logic.
        private Board _board;

        // The Cols, Rows and MineCount constants below define the dimensions for the board.
        // Change the values of these fields to alter the board size and/or mine count.
        private const int Cols = 8;
        private const int Rows = 8;
        private const int MineCount = 10;

        // The _buttonDimension fields value represents the width and height of a fields button representation in the UI in pixels.
        private readonly int _buttonDimension = 25;

        // The _timer instance keeps track of the games duration.
        private readonly Timer _timer;

        // The actual duration of the game is saved in this _elapsedSeconds field.
        private int _elapsedSeconds;

        /// <summary>
        /// Constructor of the MainWindow class.
        /// Starts the game.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            Uri iconUri = new Uri("pack://application:,,,/Resources/Images/MinesweeperIcon.ico", UriKind.RelativeOrAbsolute);
            this.Icon = BitmapFrame.Create(iconUri);

            _timer = new Timer(1000);
            InitializeTimer();

            ResetUI();

            StartGame();
        }

        /// <summary>
        /// Configures the UI of the game for a new start.
        /// All values are reset.
        /// </summary>
        public void ResetUI()
        {
            RemainingMineCount.Content = MineCount.ToString().PadLeft(2, '0');
            ElapsedTimeDisplay.Content = "000";

            FieldContainer.Width = Cols * _buttonDimension;
            FieldContainer.Height = Rows * _buttonDimension;

            Board.Width = FieldContainer.Width;
            Board.Height = FieldContainer.Height + InfoSection.Height + InfoSection.Margin.Top + InfoSection.Margin.Bottom;

            int margin = 5;
            Width = Board.Width + margin;
            Height = Board.Height + SystemParameters.CaptionHeight + margin;

            SmileyButton.Source = new BitmapImage(new Uri(@"Resources\Images\smiley-regular.png", UriKind.Relative));
        }

        /// <summary>
        /// Initializes the Board instance, prepares the timer and triggers the creation of field representing buttons.
        /// </summary>
        public void StartGame()
        {
            _board = new Board(Cols, Rows, MineCount);

            _elapsedSeconds = 0;

            DrawButtons();
        }

        /// <summary>
        /// Initializes the timer for the game.
        /// </summary>
        private void InitializeTimer()
        {
            _elapsedSeconds = 0;
            _timer.Elapsed += TimerTick;
            _timer.AutoReset = true;
            _timer.Enabled = false;
        }

        /// <summary>
        /// Handler method for clicks on the "new game" button (top-center of UI).
        /// </summary>
        /// <param name="sender">The button on which the click event occured.</param>
        /// <param name="e">Mouse click event arguments.</param>
        private void RestartGame(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
            ResetUI();
            StartGame();
        }

        /// <summary>
        /// Draws for every field on the board a button as visual representation.
        /// After the button is created, it is assigned to its corresponding field.
        /// Two click handlers - for left and right mouse click - are registered for every button.
        /// </summary>
        private void DrawButtons()
        {
            // First, clear the FieldContainer.
            // If another game was played previously, all existing buttons should be removed.
            FieldContainer.Children.Clear();

            foreach (Field field in _board.Fields)
            {
                Image buttonImage = new Image
                {
                    Source = new BitmapImage(new Uri(@"Resources\Images\button.png", UriKind.Relative))
                };

                Button button = new Button
                {
                    Name = $"Button{field.Cell.ToString()}",
                    Tag = $"{field.Cell.ToString()}",
                    Height = _buttonDimension,
                    Width = _buttonDimension,
                    BorderThickness = new Thickness(0),
                    Content = buttonImage
                };

                // The button is linked to its field.
                field.Button = button;

                // Register two click handlers for the button.
                button.Click += OnButtonClick;
                ButtonRightClickCommand.AddRightClick(button, OnButtonRightClick);

                // Finally, the button gets added to the existing WrapPanel.
                FieldContainer.Children.Add(button);
            }
        }

        /// <summary>
        /// Click handler for buttons on game board.
        /// Every button click (left mouse button) triggers execution of this handler method.
        /// </summary>
        /// <param name="sender">The button which got clicked.</param>
        /// <param name="e">Event arguments.</param>
        private void OnButtonClick(object sender, RoutedEventArgs e)
        {
            // Start the timer, if it's not running yet.
            if (!_timer.Enabled)
            {
                _timer.Start();
            }

            // Since we know the sender to be a button, we do a casting.
            Button button = (Button)sender;

            // We need to know which button exactly got clicked.
            // Therefore, we retrieve its coordinates in the form of a Cell object.
            Cell cell = GetCell(button);

            // The UncoverField method of the board object returns true if it targeted a "safe" field.
            if (_board.UncoverField(cell))
            {
                // A click on a single field can uncover multiple fields.
                // Since one of these fields could previously have been marked as "suspected mine",
                // we need to update the remaining mines count accordingly.
                UpdateRemainingMinesCount();

                // That's the winning condition:
                // There are exactly as many uncovered fields left on the board as mines exist.
                if (_board.Fields.Length - _board.UncoveredFields == MineCount)
                {
                    Win();
                }
            }
            else
            {
                // O-oh: A mine was hit! You LOSE!
                Lose();
            }
        }

        /// <summary>
        /// Extracts positional information (in the form of a cell) out of the given button.
        /// </summary>
        /// <param name="button">The button for which positional information should be extracted.</param>
        /// <returns>Positional information (Cell object) according to the buttons position on the game board.</returns>
        private Cell GetCell(Button button)
        {
            string tag = button.Tag.ToString();
            return new Cell(tag);
        }

        /// <summary>
        /// This method gets called on every tick of the timer.
        /// Actually, the count of seconds since the first click on a button gets incremented and
        /// a method for updating the display of elapsed time gets called.
        /// Since this runs in a separat thread, we need the Dispatcher to invoke the UI update.
        /// Note: Threading is an advanced toping and therefore not part of this module!
        /// </summary>
        /// <param name="sender">The timer instance which triggered this method.</param>
        /// <param name="e">Event arguments.</param>
        private void TimerTick(object sender, ElapsedEventArgs e)
        {
            _elapsedSeconds++;
            Dispatcher?.Invoke(UpdateTimerDisplay);
        }

        /// <summary>
        /// This method gets called by the timer tick event handler.
        /// Its primary purpose is to update the text label containing the count of elapsed seconds with a three-digit value.
        /// Values below 100 are padded (left) with zeroes.
        /// </summary>
        private void UpdateTimerDisplay()
        {
            ElapsedTimeDisplay.Content = _elapsedSeconds.ToString().PadLeft(3, '0');
        }

        /// <summary>
        /// Updates the display which shows the remaining mines on the game board.
        /// </summary>
        private void UpdateRemainingMinesCount()
        {
            RemainingMineCount.Content = (MineCount - _board.SuspectedMinesCount).ToString().PadLeft(2, '0');
        }

        /// <summary>
        /// Handler of secondary mouse clicks (right-click) on fields (represented by buttons).
        /// A secondary click toggles the suspect for a mine on the corresponding field.
        /// </summary>
        /// <param name="button">The button on which a secondary mouse click occured.</param>
        private void OnButtonRightClick(Button button)
        {
            Cell cell = GetCell(button);
            _board.ToggleSuspectMine(cell);
            UpdateRemainingMinesCount();
        }

        /// <summary>
        /// This method gets called if the player won the game.
        /// A player wins if all fields without mines were uncovered.
        /// </summary>
        public void Win()
        {
            _timer.Stop();
            _board.DisableAllButtons();
            SmileyButton.Source = new BitmapImage(new Uri(@"Resources\Images\smiley-win.png", UriKind.Relative));
        }

        /// <summary>
        /// This method gets called if the player lost the game.
        /// A player loses if a field containing a mine was uncovered.
        /// </summary>
        public void Lose()
        {
            _timer.Stop();
            _board.ShowAllMines();
            _board.DisableAllButtons();
            SmileyButton.Source = new BitmapImage(new Uri(@"Resources\Images\smiley-lose.png", UriKind.Relative));
        }

        /// <summary>
        /// Since the Window has style "None", this method restores the possibility to move the window on screen.
        /// It does not add actually add any relevant functionality for the game.
        /// </summary>
        /// <param name="sender">The window object on which the mouse-down event occured.</param>
        /// <param name="e">Mouse down event arguments.</param>
        public void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

    }
}