﻿using System;
using System.Collections.Generic;

namespace Minesweeper_Solution
{
    /// <summary>
    /// This class stands for the game board.
    /// On the board, fields are aligned in rows and columns. In the UI, the fields are represented by buttons.
    /// </summary>
    class Board
    {
        /// <summary>
        /// Column count for game board.
        /// </summary>
        private readonly int _colCount;

        /// <summary>
        /// Row count for game board.
        /// </summary>
        private readonly int _rowCount;

        /// <summary>
        /// Total count of mines on the game board.
        /// </summary>
        private readonly int _mineCount;

        /// <summary>
        /// Count of currently uncovered fields on the board.
        /// Information is needed to evaluate the winning condition.
        /// </summary>
        public int UncoveredFields { get; private set; }

        /// <summary>
        /// Count of fields which the player marked as potentially mine containing.
        /// </summary>
        public int SuspectedMinesCount { get; private set; }

        /// <summary>
        /// Two-dimensional array containing all fields of the game board.
        /// The first dimension is for columns, the second dimension is for rows.
        /// </summary>
        public Field[,] Fields { get; }

        /// <summary>
        /// The constructor initializes the board dimensions and the mine count.
        /// It is also responsible for initiating the creation of fields according to the board dimensions
        /// and the seeding of the mines.
        /// </summary>
        /// <param name="colCount">Desired column count of board.</param>
        /// <param name="rowCount">Desired row count of board.</param>
        /// <param name="mineCount">Desired mine count of board.</param>
        public Board(int colCount, int rowCount, int mineCount)
        {
            _colCount = colCount;
            _rowCount = rowCount;
            _mineCount = mineCount;

            Fields = new Field[colCount, rowCount];

            CreateFields();
            SeedMines();
        }

        /// <summary>
        /// Creates all Field objects on the game board.
        /// </summary>
        private void CreateFields()
        {
            for (int column = 0; column < _colCount; column++)  // loop over all columns
            {
                for (int row = 0; row < _rowCount; row++)  // loop over all rowCount
                {
                    Fields[column, row] = new Field(new Cell(column, row));
                }
            }
        }

        /// <summary>
        /// Randomly positions mines on the board.
        /// </summary>
        private void SeedMines()
        {
            // Count of the already successfully planted mines
            int seededMines = 0;

            Random randomNumberGenerator = new Random();

            // Do-while loop for the actual seeding.
            // We need to repeat the seeding process until all mines have been placed on a different field.
            do
            {
                // Generate two different random numbers for the column and row position.
                // Note: Upper boundary for Next method is exclusive!
                int col = randomNumberGenerator.Next(0, _colCount);
                int row = randomNumberGenerator.Next(0, _rowCount);

                // Retrieve the field at the randomly generated position
                Field field = Fields[col, row];

                // Check if the field already holds a mine.
                if (!field.IsMine)
                {
                    // Plant mine...
                    field.IsMine = true;

                    // ...and update surrounding fields mine count.
                    IncrementNeighboursMineCount(field);

                    // Increment count of successfully planted mines.
                    seededMines++;
                }
            } while (seededMines < _mineCount);
        }

        /// <summary>
        /// Increment the count of mines of all fields which are directly next to the given field.
        /// This method gets called, when a mine was planted on the given field.
        /// </summary>
        /// <param name="field">Field on which a mine was planted.</param>
        private void IncrementNeighboursMineCount(Field field)
        {
            foreach (Field surroundingField in GetAdjoiningFields(field))
            {
                surroundingField.SurroundingMineCount++;
            }
        }

        /// <summary>
        /// Finds and returns all fields which are directly next to given field.
        /// "Next to" is interpreted in horizontal, vertical and diagonal meaning.
        /// </summary>
        /// <param name="field">The field for which the adjoining field should be calculated.</param>
        /// <returns>A list of fields which are next to the given field.</returns>
        private List<Field> GetAdjoiningFields(Field field)
        {
            // Calculate boundaries for column and row number surrounding the given field.
            // All values must be >= 0 and < than the count of columns or rowCount respectively.
            int lowerCol = Math.Max(0, field.Column - 1);
            int upperCol = Math.Min(field.Column + 1, _colCount - 1);
            int lowerRow = Math.Max(0, field.Row - 1);
            int upperRow = Math.Min(field.Row + 1, _rowCount - 1);

            // To this list, the adjoining fields will be added
            List<Field> adjoiningFields = new List<Field>();

            // Loop over adjoining columns
            for (int col = lowerCol; col <= upperCol; col++)
            {
                // Loop over adjoining rowCount
                for (int row = lowerRow; row <= upperRow; row++)
                {
                    // Get adjoining field from array of fields.
                    Field adjoiningField = Fields[col, row];

                    // If calculated adjoining field is NOT the given field itself...
                    if (!adjoiningField.Cell.IsSamePosition(field.Cell))
                    {
                        // ...add it to the list of surrounding fields.
                        adjoiningFields.Add(adjoiningField);
                    }
                }
            }

            return adjoiningFields;
        }

        /// <summary>
        /// Returns the fields which are next to the given field, do not contain a mine and are still uncovered.
        /// </summary>
        /// <param name="field">The field for which the safe fields should be found.</param>
        /// <returns>List of adjoining, safe fields.</returns>
        public List<Field> GetAdjoiningSafeFields(Field field)
        {
            // Start with the list of all adjoining fields.
            List<Field> adjoiningSafeFields = GetAdjoiningFields(field);

            foreach (Field adjoiningField in GetAdjoiningFields(field))
            {
                // Check for fields which are either uncovered yet or do contain a mine...
                if (adjoiningField.IsUncovered || adjoiningField.IsMine)
                {
                    // ...and remove them from the list of adjoining, neutral fields.
                    adjoiningSafeFields.Remove(adjoiningField);
                }
            }

            return adjoiningSafeFields;
        }

        /// <summary>
        /// Uncover a field.
        /// A field gets uncovered, when either its button got clicked or an adjoining field triggered the uncovering.
        /// Vice versa, if the field to uncover is a neutral field, it triggers the uncovering of surrounding fields.
        /// This might lead to recursive method calls!
        /// The return value of the method indicates whether it was a safe field (true) or a mine was hit (false).
        /// </summary>
        /// <param name="cell">The positional representation of the field to uncover.</param>
        /// <returns>True, if the uncovered field was a "safe" field, False otherwise.</returns>
        public bool UncoverField(Cell cell)
        {
            Field field = Fields[cell.Column, cell.Row];
            UncoveredFields++;

            // Decrement the count of suspected mine fields,
            // if the field to uncover was suspected to contain a mine.
            if (field.IsSuspectedMine)
            {
                SuspectedMinesCount--;
            }

            field.Uncover();

            // If the uncovered field was neutral...
            if (field.IsNeutral)
            {
                foreach (Field adjoiningField in GetAdjoiningSafeFields(field))
                {
                    if (!adjoiningField.IsUncovered)
                    {
                        // ...uncover all adjoining neutral fields.
                        // This is a recursive method call.
                        UncoverField(adjoiningField.Cell);
                    }
                }
            }

            // Indicate to the caller whether this was a safe field uncovered.
            return !field.IsMine;
        }

        /// <summary>
        /// Toggle the suspect-mine state.
        /// </summary>
        /// <param name="cell"></param>
        public void ToggleSuspectMine(Cell cell)
        {
            Field field = Fields[cell.Column, cell.Row];
            field.ToggleSuspectMine();
            SuspectedMinesCount += field.IsSuspectedMine ? 1 : -1;
        }

        /// <summary>
        /// Disables the buttons for all fields on the board.
        /// </summary>
        public void DisableAllButtons()
        {
            foreach (Field field in Fields)
            {
                if (field.Button.IsEnabled)
                {
                    field.Button.IsEnabled = false;
                }
            }
        }

        /// <summary>
        /// Uncovers all fields containing a mine.
        /// </summary>
        public void ShowAllMines()
        {
            foreach (Field field in Fields)
            {
                if (field.IsMine)
                {
                    field.Uncover();
                }
            }
        }
    }
}
